This image is used to provide an NGINX proxy in front of different services while
*in development!*

Based on the amount and type of services, select the flavor that you need (see list below)
and link your services with some pre determined names. For backend, use `backend-0X` and
for frontend services use `app-0X`.

All frontend services have livereload and browsersync capabilities enabled if necessary. For all
`app-0X`, they will receive traffic in the usual ports 9000 for web requests, 3000 for browsersync and
35729 for livereload.

Their proxy port will depend on the service being used. For app-01 the mapping is 9000, 3000 and 6000. For app-02 is 9001, 3001 and 6001.

All `backend-0X` receive plain HTTP traffic on port 8000 and can be proxied from 8000 or 8001 for `backend-02`. Both backend also support ssl
at ports 8443 and 8444, that is not relayed over to the backend server (Client-(https)->NGINX-(http)->Server). Certificates are
looked for in `/etc/nginx/certs/backend0X/` folders, that are exposed as volumes. *These are not enabled by default* and should be linked
in `conf.d` from `conf.availables` by your Dockerfile:

```Dockerfile
FROM octobotdev/nginx-dev:backend-01-app-01
RUN rm /etc/nginx/conf.d/backend01.conf &&\
    ln -s /etc/nginx/conf.availables/backend01https.conf /etc/nginx/conf.d/backend01.conf
```

An usage example in a docker-compose environment would be:

```
nginx-proxy:
    restart: unless-stopped
    image: octobotdev/nginx-proxy-dev:backend-01-app-02
    links:
      - django:backend-01
      - frontend:app-01
      - backoffice:app-02
    ports:
      # Backend
      - "8000:8000"
      # Frontend
      - "9000:9000"
      # Frontend browsersync
      - "3000:3000"
      # Frontend livereload
      - "6000:6000"
      # Backoffice
      - "9001:9001"
      # Backoffice browsersync
      - "3001:3001"
      # Backoffice livereload
      - "6001:6001"

```

Available flavors are:
- base : No config files are enabled.
- backend-01-app-00 : One backend site enabled, none frontend sites enabled.
- backend-01-app-01 : One backend site enabled, one frontend site enabled.
- backend-01-app-02 : One backend site enabled, two frontend sites enabled.
- backend-01-app-00 : One backend site enabled, three frontend sites enabled.
- backend-02-app-00 : Two backend site enabled, none frontend sites enabled.
- backend-02-app-01 : Two backend site enabled, one frontend site enabled.
- backend-02-app-02 : Two backend site enabled, two frontend sites enabled.
- backend-02-app-00 : Two backend site enabled, three frontend sites enabled.
